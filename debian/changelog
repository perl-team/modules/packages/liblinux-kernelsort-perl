liblinux-kernelsort-perl (0.01-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 22 Oct 2022 16:51:07 -0000

liblinux-kernelsort-perl (0.01-4) unstable; urgency=medium

  * Take over for the Debian Perl Group on maintainer's request
    (https://lists.debian.org/debian-perl/2020/12/msg00003.html)
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza). Changed: Homepage field changed to
    metacpan.org URL; Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Maximilian Wilhelm
    <max@rfc2324.org>); Maximilian Wilhelm <max@rfc2324.org> moved to
    Uploaders; Replace (build-)dependency on ancient version of perl
    with a dependency on perl without a version (as permitted by Debian
    Policy 3.8.3).
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Use ${perl:Depends} in Depends.
  * Bump debhelper-compat to 13.
  * debian/watch: use uscan version 4.
  * Set Homepage field in Source rather than Binary package.
  * debian/rules: use dh(1).
  * debian/copyright: use Copyright-Format 1.0.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Dec 2020 18:40:21 +0100

liblinux-kernelsort-perl (0.01-3) unstable; urgency=low

  * Bumped standards version to 3.9.4.
  * Migrate package from dpatch to format '3.0 (quilt)' (Closes: #664440)
  * Fixed lintian warnings and errors.

 -- Maximilian Wilhelm <max@rfc2324.org>  Tue, 04 Jun 2013 14:42:33 +0200

liblinux-kernelsort-perl (0.01-2) unstable; urgency=low

  * Fixed lintian warning 'description-contains-homepage' and moved homepage
    from Description to Homepage field for the package.
  * Bumped Standards-Version to 3.7.3
  * Updated debian/copyright to fit copyright information from upstream README

 -- Maximilian Wilhelm <max@rfc2324.org>  Sat, 29 Dec 2007 18:04:39 +0100

liblinux-kernelsort-perl (0.01-1) unstable; urgency=low

  * Initial release (Closes: #401940)
  * Fixed documentation in KernelSort.pm
  * Accept leading 'v' in version string.
  * Acceept trailing '-tree' in version string.
  * Accept 4 digit version (e.g. 2.6.18.2)

 -- Maximilian Wilhelm <max@rfc2324.org>  Sun, 17 Dec 2006 14:30:40 +0100
